package com.clicklabs.tuli;

public class Data
{
    private String sessionCost;

    private String status;

    private String location;

    private String extendTime;

    private String __v;

    private String[] ratingByTutor;

    private String[] ratingByStudent;

    private CourseId courseId;

    private String endTime;

    private String startTime;

    private String[] topics;

    private String _id;

    private String tutorId;

    private StudentId studentId;

    public String getSessionCost ()
    {
        return sessionCost;
    }

    public void setSessionCost (String sessionCost)
    {
        this.sessionCost = sessionCost;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getLocation ()
    {
        return location;
    }

    public void setLocation (String location)
    {
        this.location = location;
    }

    public String getExtendTime ()
    {
        return extendTime;
    }

    public void setExtendTime (String extendTime)
    {
        this.extendTime = extendTime;
    }

    public String get__v ()
    {
        return __v;
    }

    public void set__v (String __v)
    {
        this.__v = __v;
    }

    public String[] getRatingByTutor ()
    {
        return ratingByTutor;
    }

    public void setRatingByTutor (String[] ratingByTutor)
    {
        this.ratingByTutor = ratingByTutor;
    }

    public String[] getRatingByStudent ()
    {
        return ratingByStudent;
    }

    public void setRatingByStudent (String[] ratingByStudent)
    {
        this.ratingByStudent = ratingByStudent;
    }

    public CourseId getCourseId ()
    {
        return courseId;
    }

    public void setCourseId (CourseId courseId)
    {
        this.courseId = courseId;
    }

    public String getEndTime ()
    {
        return endTime;
    }

    public void setEndTime (String endTime)
    {
        this.endTime = endTime;
    }

    public String getStartTime ()
    {
        return startTime;
    }

    public void setStartTime (String startTime)
    {
        this.startTime = startTime;
    }

    public String[] getTopics ()
    {
        return topics;
    }

    public void setTopics (String[] topics)
    {
        this.topics = topics;
    }

    public String get_id ()
    {
        return _id;
    }

    public void set_id (String _id)
    {
        this._id = _id;
    }

    public String getTutorId ()
    {
        return tutorId;
    }

    public void setTutorId (String tutorId)
    {
        this.tutorId = tutorId;
    }

    public StudentId getStudentId ()
    {
        return studentId;
    }

    public void setStudentId (StudentId studentId)
    {
        this.studentId = studentId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [sessionCost = "+sessionCost+", status = "+status+", location = "+location+", extendTime = "+extendTime+", __v = "+__v+", ratingByTutor = "+ratingByTutor+", ratingByStudent = "+ratingByStudent+", courseId = "+courseId+", endTime = "+endTime+", startTime = "+startTime+", topics = "+topics+", _id = "+_id+", tutorId = "+tutorId+", studentId = "+studentId+"]";
    }
}

