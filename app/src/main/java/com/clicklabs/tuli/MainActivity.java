package com.clicklabs.tuli;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends Activity {
    final String END_POINT = "http://52.24.184.70:8000";
    MyBaseAdapter adapter;
    String authKey = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1YmY1YjQ0ZjM1YjgyYTI1N2I5NmM0MyIsImxhc3RMb2dpbiI6IjIwMTUtMDgtMDRUMTI6Mjg6MjMuNzg0WiIsIm1lc3NhZ2UiOiJMb2dpbiBBY2Nlc3MgVG9rZW4iLCJpYXQiOjE0Mzg2OTEzMDN9.dOzApg-7fcO4Xiy7nLmPs7rx-SkTwCiNkf_DQZlcjog";

    List<Data> sessionsList;
    ListView sessionsListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        sessionsListView = (ListView) findViewById(R.id.list);

        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(END_POINT).build();
        WebServices webServices = restAdapter.create(WebServices.class);
        webServices.getAllSessions(authKey, new Callback<TutorSession>() {
            @Override
            public void success(TutorSession tutorSession, Response response) {
                Toast.makeText(MainActivity.this, "Success", Toast.LENGTH_SHORT).show();
                sessionsList = tutorSession.getData();
                adapter = new MyBaseAdapter(MainActivity.this, sessionsList);
                sessionsListView.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MainActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
            }
        });


    }

}
