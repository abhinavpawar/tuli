package com.clicklabs.tuli;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MyBaseAdapter extends BaseAdapter {
    final String authKey="Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjU1YmY1YjQ0ZjM1YjgyYTI1N2I5NmM0MyIsImxhc3RMb2dpbiI6IjIwMTUtMDgtMDRUMTI6Mjg6MjMuNzg0WiIsIm1lc3NhZ2UiOiJMb2dpbiBBY2Nlc3MgVG9rZW4iLCJpYXQiOjE0Mzg2OTEzMDN9.dOzApg-7fcO4Xiy7nLmPs7rx-SkTwCiNkf_DQZlcjog";
    final String END_POINT = "http://52.24.184.70:8000";
    private LayoutInflater mInflater;
    private List<Data> mTutorSessions;
    int itemPosition;
    public MyBaseAdapter(Context context, List<Data> mTutorSessions) {
        mInflater = LayoutInflater.from(context);
        this.mTutorSessions = mTutorSessions;
    }

    @Override
    public int getCount() {
        return mTutorSessions.size();
    }

    @Override
    public Data getItem(int position) {

        return mTutorSessions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        //itemPosition=position;
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.row_layout, parent, false);
            holder = new ViewHolder();
            holder.reschedule = (Button) view.findViewById(R.id.rescheduleButton);
            holder.cancel = (Button) view.findViewById(R.id.cancelButton);
            holder.courseName = (TextView) view.findViewById(R.id.courseName);
            holder.description = (TextView) view.findViewById(R.id.text2);
            holder.studentName = (TextView) view.findViewById(R.id.studentName);
            holder.timeButton1 = (Button) view.findViewById(R.id.timeButton1);
            holder.timeButton15 = (Button) view.findViewById(R.id.timeButton15);
            holder.timeButton30 = (Button) view.findViewById(R.id.timeButton30);
            holder.timeButton45 = (Button) view.findViewById(R.id.timeButton45);
            holder.timeLeft = (TextView) view.findViewById(R.id.timeRemaining);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        Data data = getItem(position);
        String courseName = data.getCourseId().getTitle();
        String studentName = (data.getStudentId().getFirstName()).concat(data.getStudentId().getLastName());
        final String sessionId=data.get_id();
        // String description;
        String startTime = data.getStartTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        Date startDate = null;
        try {
            startDate = sdf.parse(startTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date endDate = null;
        String endTime = data.getEndTime();
        try {
            endDate = sdf.parse(endTime.replaceAll(":(?=..$)", ""));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diffHours = 0;
        try {
            long diff = endDate.getTime() - startDate.getTime();
            diffHours = diff / (60 * 60 * 1000);

        } catch (NullPointerException npe) {
        }

        holder.courseName.setText(courseName);
        holder.studentName.setText(studentName);
        holder.timeLeft.setText(Objects.toString(diffHours, null) + "Hr(s)");

        View.OnClickListener extendTime = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RestAdapter restAdapter=new RestAdapter.Builder().setEndpoint(END_POINT).build();
                WebServices webServices=restAdapter.create(WebServices.class);
                switch (v.getId()) {
                    case R.id.timeButton15: {
                        Data data = getItem(position);
                        String sessionIdTemp=data.get_id();
                        webServices.extendSession(authKey, sessionId, 0.25, new Callback<ExtendTimeResponse>() {
                            @Override
                            public void success(ExtendTimeResponse extendTimeResponse, Response response) {

                            }

                            @Override
                            public void failure(RetrofitError error) {

                            }
                        });
                    }
                    case R.id.timeButton30: {
                        webServices.extendSession(authKey, sessionId, 0.5, new Callback<ExtendTimeResponse>() {
                            @Override
                            public void success(ExtendTimeResponse extendTimeResponse, Response response) {

                            }

                            @Override
                            public void failure(RetrofitError error) {

                            }
                        });
                    }
                    case R.id.timeButton45: {
                        webServices.extendSession(authKey, sessionId, 0.75, new Callback<ExtendTimeResponse>() {
                            @Override
                            public void success(ExtendTimeResponse extendTimeResponse, Response response) {

                            }

                            @Override
                            public void failure(RetrofitError error) {

                            }
                        });
                    }
                    case R.id.timeButton1: {
                        webServices.extendSession(authKey, sessionId, 1.0, new Callback<ExtendTimeResponse>() {
                            @Override
                            public void success(ExtendTimeResponse extendTimeResponse, Response response) {

                            }

                            @Override
                            public void failure(RetrofitError error) {

                            }
                        });
                    }
                }
            }
        };

    holder.cancel.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            RestAdapter restAdapter=new RestAdapter.Builder().setEndpoint(END_POINT).build();
            WebServices webServices=restAdapter.create(WebServices.class);
            webServices.deleteSession(authKey, sessionId, new Callback<com.clicklabs.tuli.Response>() {
                @Override
                public void success(com.clicklabs.tuli.Response response, Response response2) {

                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }
    });
    holder.timeButton15.setOnClickListener(extendTime);
    holder.timeButton30.setOnClickListener(extendTime);
    holder.timeButton45.setOnClickListener(extendTime);
    holder.timeButton1.setOnClickListener(extendTime);

    return view;
}

private class ViewHolder {
    public Button reschedule;
    public Button cancel;
    public TextView courseName;
    public TextView studentName;
    public TextView description;
    public Button timeButton15;
    public Button timeButton30;
    public Button timeButton45;
    public Button timeButton1;
    public TextView timeLeft;
}

public void getPosition(Bundle b){
    itemPosition=(int)b.get("position");
}
}

