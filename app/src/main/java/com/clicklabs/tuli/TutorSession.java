package com.clicklabs.tuli;

import java.util.List;

public class TutorSession
{
    private String message;

    private List<Data> data;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public List<Data> getData ()
    {
        return data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", data = "+data+"]";
    }
}