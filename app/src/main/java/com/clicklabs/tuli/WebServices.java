package com.clicklabs.tuli;

import retrofit.Callback;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.PUT;
import retrofit.http.Query;

/**
 * Created by click on 8/6/15.
 */
public interface WebServices {

    @GET("/api/tutor/mySessions")
    void getAllSessions(@Header("Authorization") String authKey, Callback<TutorSession> response);


    @PUT("/api/tutor/session/extend")
    void extendSession(@Header("Authorization") String authKey, @Query("sessionId") String sessionId, @Query("extendTime") Double extendTime, Callback<ExtendTimeResponse> response);

    @DELETE("/api/tutor/session/cancel")
    void deleteSession(@Header("Authorization") String authKey,@Query("sessionId") String sessionId,Callback<Response> response);
}
